package com.guitarFactor.userService;

import com.guitarFactor.model.User;
import com.guitarFactor.userDAO.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private UserDAO userDAO;

    @Autowired
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Transactional
    public List<User> getListOfUsers() {
        return this.userDAO.getListOfUsers();
    }

    @Transactional
    public List<User> getListOfUsersByPage(int pageId) {
        return this.userDAO.getListOfUsersByPage(pageId);
    }

    @Transactional
    public void addUser(User user) {
        this.userDAO.addUser(user);
    }

    @Transactional
    public void updateUser(User user) {
        this.userDAO.updateUser(user);
    }

    @Transactional
    public void deleteUser(int id) {
        this.userDAO.deleteUser(id);
    }

    @Transactional
    public User getUserById(int id) {
        return this.userDAO.getUserById(id);
    }

    @Transactional
    public List<String> searchByAge(Double age) {
        return this.userDAO.searchByAge(age);
    }
}

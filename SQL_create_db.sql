CREATE DATABASE test
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE test.user (
  id int(8) NOT NULL AUTO_INCREMENT,
  name varchar(25) NOT NULL,
  age int(11) NOT NULL,
  is_admin bit(1) DEFAULT b'0',
  created_date timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
);

INSERT INTO test.user (name, age, is_admin, created_date)
  VALUES ('Vyacheslav', 27, 1, NOW());

INSERT INTO test.user (name, age, is_admin, created_date)
  VALUES ('Maria', 25, 0, NOW());

INSERT INTO test.user (name, age, is_admin, created_date)
  VALUES ('Nadezda', 49, 0, NOW());

INSERT INTO test.user (name, age, is_admin, created_date)
  VALUES ('Alexey', 54, 0, NOW()); 

INSERT INTO test.user (name, age, is_admin, created_date)
  VALUES ('Captain Bobrov', 100, 0, NOW());

INSERT INTO test.user (name, age, is_admin, created_date)
  VALUES ('Test', 0, 0, NOW()); 


package com.guitarFactor.userService;

import com.guitarFactor.model.User;

import java.util.List;


public interface UserService {
    public List<User> getListOfUsers();
    public List<User> getListOfUsersByPage(int pageId);
    public void addUser(User user);
    public void updateUser(User user);
    public void deleteUser(int id);
    public User getUserById(int id);
    public List<String> searchByAge(Double age);
}

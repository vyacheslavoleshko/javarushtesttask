<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>JavaRush Test</title>
    <style type="text/css">
        body {
            background-color: cornsilk;
        }
        table {
            border: 3px solid black;
            margin:6px;
            border-spacing: 0;
            border-collapse: collapse;
            text-align: center;
        }
        td {
            border: 2px solid black;
            border-collapse: collapse;
            font-family: Arial, sans-serif;
            font-size: 14px;
            padding: 10px;
            overflow: hidden;
            word-break: normal;
            background-color: #fff;
        }

        th {
            border: 2px solid black;
            font-family: Arial, sans-serif;
            font-size: 16px;
            font-weight: bold;
            padding: 10px;
            overflow: hidden;
            word-break: normal;
            background-color: #f0f0f0;
        }
        table.noborder{
            border: none;
            text-align: left;
        }
        td.noborder {
            border: none;
            font-family: Arial, sans-serif;
            font-size: 14px;
            padding: 10px;
            overflow: hidden;
            word-break: normal;
            width: 90px;
            background-color: lightcyan;
        }
        div.userstable {
            position: absolute;
            top: 10px;
            left: 370px;
            background-color:lightcyan;
        }
        div.center{
            text-align: center;
        }

    </style>
</head>
<body>

<c:url var="add" value="/users/add/"></c:url>

<form:form method="POST" action="${add}" modelAttribute="user">
    <table class="noborder">
        <td colspan="2" class="noborder" align="center"> <h1>Add/Edit user</h1></td>
        <c:if test="${!empty user.name}">
            <tr>
                <td class="noborder">
                    <form:label path="id">
                        <spring:message text="ID"/>
                    </form:label>
                </td>
                <td class="noborder">
                    <form:input path="id" readonly="true" size="8" disabled="true"/>
                    <form:hidden path="id"/>
                </td>
            </tr>
        </c:if>
        <tr>
            <td class="noborder">
                <form:label path="name">
                    <spring:message text="Name"/>
                </form:label>
            </td>
            <td class="noborder">
                <form:input path="name" placeholder="type name"/>
            </td>
        </tr>
        <tr>
            <td class="noborder">
                <form:label path="age">
                    <spring:message text="Age"/>
                </form:label>
            </td>
            <td class="noborder">
                <form:input path="age" placeholder="type age"/>
            </td>
            </tr>
        <tr>
            <td class="noborder">
                <form:label path="isAdmin">
                    <spring:message text="Add admin privelegies to user? "/>
                </form:label>
            </td>
            <td class="noborder">
                <form:radiobutton path="isAdmin" value="true" class="radio onRadio"/>
                <label class="lblOn">Yes</label>
                <form:radiobutton path="isAdmin" value="false" class="radio offRadio"/>
                <label class="lblOff">No</label>
            </td>
        </tr>
        <c:if test="${!empty user.name}">
            <tr>
                <td class="noborder">
                    <form:label path="createdDate">
                        <spring:message text="User was created: "/>
                    </form:label>
                </td>
                <td class="noborder">
                    <form:input path="createdDate" readonly="true" size="15" disabled="true"/>
                </td>
            </tr>
        </c:if>
        <tr>
            <td colspan="2" class="noborder" align="center">
                <c:if test="${!empty user.name}">
                    <input type="submit"
                           value="<spring:message text="Edit Person"/>"/>
                </c:if>
                <c:if test="${empty user.name}">
                    <input type="submit"
                           value="<spring:message text="Add Person"/>"/>
                </c:if>
            </td>
        </tr>
    </table>
</form:form>

<br>
<hr>
<br>

<c:url var="search" value="/users/search/"/>
<form:form method="POST" action="${search}">
<table class="noborder">
    <tr><td class="noborder" colspan="2">Search users under age:</td></tr>
    <tr>
        <td class="noborder">
            <input type="text" name="age" placeholder="type age: e.g. 55.5"/>
        </td>
        <td class="noborder">
            <input type="submit"
                   value="<spring:message text="Search"/>"/>
            </form:form>
        </td>
    </tr>
</table>
<div class="userstable">
    <table>
        <tr>
            <th width="30">Id</th>
            <th width="120">Name</th>
            <th width="60">Age</th>
            <th width="60">Admin</th>
            <th width="60">Edit</th>
            <th width="60">Delete</th>
        </tr>
        <c:forEach items="${listOfUsers}" var="user">
            <tr>
                <td>${user.id}</td>
                <td>${user.name}</td>
                <td>${user.age}</td>
                <c:if test="${user.isAdmin==true}">
                    <td><img src="<c:url value='/resources/images/tick.jpg'/>"/></td>

                </c:if>
                <c:if test="${user.isAdmin==false}">
                    <td></td>
                </c:if>
                <td><a href="<c:url value='/users/edit/${user.id}'/> "> Edit </a></td>
                <td><a href="<c:url value='/users/delete/${user.id}'/> "> Delete </a></td>
            </tr>
        </c:forEach>
    </table>
    <div class="center">
        <c:forEach begin="0" end="${totalNumberOfPages}" var="page">
           <a href="<c:url value='/users/${page}'/> "> ${page} &nbsp; </a>
        </c:forEach>
    </div>
</div>

</body>
</html>

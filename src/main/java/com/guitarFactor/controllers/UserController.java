package com.guitarFactor.controllers;

import com.guitarFactor.model.User;
import com.guitarFactor.userDAO.UserDAOImpl;
import com.guitarFactor.userService.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;


@Controller
public class UserController {

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String start(){
        return "startPage";
    }

    @RequestMapping(value = "/users/{pageId}", method = RequestMethod.GET)
    public String listUsersByPage(@PathVariable("pageId") int pageId, Model model) {
        model.addAttribute("user", new User());
        List<User> listOfUsersOnPage = userService.getListOfUsersByPage(pageId);
        // users to display
        model.addAttribute("listOfUsers", listOfUsersOnPage);

        // count how many pages do we need
        List<User> listOfAllUsers = userService.getListOfUsers();
        int totalNumberOfPages = (listOfAllUsers.size()-1) / UserDAOImpl.USERS_ON_ONE_PAGE;
        model.addAttribute("totalNumberOfPages", totalNumberOfPages);
        return "users";
    }

    @RequestMapping(value = "/users/add/", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("user") User user, BindingResult result) {
        if (result.hasErrors()) {
            return "redirect:/users/0";
        }
        user.setCreatedDate(new Date());
        if (user.getId() == 0) {
            userService.addUser(user);
            return "addedPage";
        } else {
            userService.updateUser(user);
            return "redirect:/users/0";
        }
    }

    @RequestMapping("users/delete/{id}")
    public String deleteUser(@PathVariable("id") int id) {
        userService.deleteUser(id);
        return "redirect:/users/0";
    }

    @RequestMapping("users/edit/{id}")
    public String editUser(@PathVariable("id") int id, Model model) {
        model.addAttribute("user", userService.getUserById(id));
        model.addAttribute("listOfUsers", userService.getListOfUsersByPage(0));
        return "users";
    }

    @RequestMapping("users/search/")
    public String searchUser(HttpServletRequest request, Model model) {
        String age = request.getParameter("age");
        Double inputAge;
        try {
            inputAge = Double.valueOf(age);
        } catch (NumberFormatException e) {
            return "searchResults";
        }
        List<String> userNamesFilteredByAge = userService.searchByAge(inputAge);
        model.addAttribute("numberOfMatchesFound", userNamesFilteredByAge.size());
        model.addAttribute("age", age);
        model.addAttribute("searchByAgeResult", userNamesFilteredByAge);
        return "searchResults";
    }

}



<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Added Succesfully!</title>
    <style type="text/css">
        body {
            background-color: cornsilk;
        }
    </style>
</head>
<body>
<h2>${user.name}</h2> was added succesfully!
    <c:url var="home" value="/users/0" />
    <p><a href="<c:url value='${home}'/> "> <img src="<c:url value='/resources/images/back.png'/>"/> Back</a></p>
</body>
</html>

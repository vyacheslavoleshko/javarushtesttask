package com.guitarFactor.userDAO;

import com.guitarFactor.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDAOImpl implements UserDAO {

    public static final int USERS_ON_ONE_PAGE=5;

    private SessionFactory sessionFactory;

    private static final Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }

    public List<User> getListOfUsers() {
        logger.info("Got list of users");
        return getCurrentSession().createQuery("from User").list();
    }

    public List<User> getListOfUsersByPage(int pageId) {
        List<User> allUsers = getListOfUsers();
        logger.info("Got list of users by page");

        int startUserId = USERS_ON_ONE_PAGE * pageId;
        int endUserId;
        if ((startUserId + USERS_ON_ONE_PAGE)>allUsers.size()){
            endUserId = allUsers.size();
        } else {
            endUserId = startUserId + USERS_ON_ONE_PAGE;
        }
        List<User> listOfUsersByPage = new ArrayList<User>();
        for (int i = startUserId; i < endUserId; i++) {
            listOfUsersByPage.add(allUsers.get(i));
        }
        return listOfUsersByPage;
    }

    public void addUser(User user) {
        getCurrentSession().persist(user);
        logger.info("User added successfully");
    }

    public void updateUser(User user) {
        getCurrentSession().update(user);
        logger.info("User updated successfully");
    }

    public User getUserById(int id) {
        logger.info("User retrieved by id successfully");
        return (User) getCurrentSession().load(User.class, id);
    }

    public void deleteUser(int id) {
        Session session = getCurrentSession();
        User user = (User) session.load(User.class, new Integer(id));
        if (user != null) {
            session.delete(user);
        }
        logger.info("User deleted successfully");
    }

    public List<String> searchByAge(Double age) {
        logger.info("Starting search for users under " + age + " years");
        List<User> listOfUsers = getListOfUsers();
        List<String> namesOfFilteredByAgeUsers = new ArrayList<String>();
        for (User user : listOfUsers) {
            if (user.getAge() <= age) {
                namesOfFilteredByAgeUsers.add(user.getName());
            }
        }
        logger.info("Search performed successfully");
        return namesOfFilteredByAgeUsers;
    }
}

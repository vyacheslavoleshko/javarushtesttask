<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>Search Results</title>
    <style type="text/css">
        body {
            background-color: cornsilk;
        }
        table {
            border: none;
            margin: 20px;
            padding: 10px;
            border-spacing: 0;
            text-align: left;
            background-color: lightcyan;
        }
    </style>
</head>
<body>
<c:url var="search" value="/users/search/"/>
<form:form method="POST" action="${search}">

<table>
    <tr>
        <td colspan="2">Search users under age:</td>
    </tr>
    <tr>
        <td>
            <input type="text" name="age" placeholder="type age: e.g. 55.5" />
        </td>
        <td>
            <input type="submit"
                   value="<spring:message text="Search"/>"/>
        </td>
    </tr>
        </form:form>
</table>

<table>
    <tr>
        <th>
            <h3>Search results</h3>
        </th>
    </tr>
    <tr>
        <td>
        <c:if test="${!empty searchByAgeResult}">
            <h4>Found ${numberOfMatchesFound} users under ${age} years</h4>

        <c:forEach items="${searchByAgeResult}" var="userName">
            <tr>
                <td>${userName}</td>
            </tr>

        </c:forEach>

        </c:if>

<c:if test="${empty searchByAgeResult}">
    <p>Users not found</p>
</c:if>
</table>

<c:url var="home" value="/users/0" />
<p><a href="<c:url value='${home}'/> "> <img src="<c:url value='/resources/images/back.png'/>"/> Back</a></p>
</body>
</html>
